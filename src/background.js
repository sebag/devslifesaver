var getLocation = function(href) {
    var l = document.createElement("a");
    l.href = href;
    return l.host;
};

function updateIcon(name, id_tab) {
    chrome.browserAction.setIcon({path:"icon" + name + ".png", tabId: id_tab});    
}

var display_warn = function(url_current, id_tab){
    console.log("searching for"+url_current);
    chrome.storage.local.get('devssaverurls', function(result){
        console.log(result);
        if (result.devssaverurls !== undefined) {
            var index = result.devssaverurls.indexOf(url_current);
            if (index > -1) {
                    updateIcon('prod', id_tab);
                    chrome.tabs.executeScript({
                            file: 'jquery-2.1.1.js'
                        }, function(){
                            chrome.tabs.executeScript({
                                    file: 'work.js'
                                }, function(){console.log("injection done")});
                    });                
            } else {
                console.log("Usuwanie napisu");
                    updateIcon('ok', id_tab);
                    chrome.tabs.executeScript({
                            file: 'jquery-2.1.1.js'
                        }, function(){
                            chrome.tabs.executeScript({
                                    file: 'delete.js'
                                }, function(){console.log("injection done")});
                    });
                
            }
        }
    });
};


chrome.tabs.onActivated.addListener(function (activeInfo) {
    console.log("tab activated changed");
    var id_tab = activeInfo.tabId;
    chrome.tabs.get(id_tab,function(tab){
        if (tab.url){
            console.log("tab activated changed current tab is "+tab.url);
            var locurl = getLocation(tab.url);
            display_warn(locurl, id_tab);
        }
        
    });
    
    display_warn(locurl);
});
chrome.tabs.onCreated.addListener(function (tab){
    console.log("tab created");
    if (tab.url){
        var locurl = getLocation(tab.url);
        display_warn(locurl,tab.id);
    }
});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    console.log("tab updated");
    if(changeInfo.url) {
        var locurl = getLocation(changeInfo.url);
        display_warn(locurl,tabId);
    }
});


chrome.browserAction.onClicked.addListener(function(tab) {
    var urltoset=getLocation(tab.url);
    chrome.storage.local.get('devssaverurls', function(result){
        console.log("inside");
        if (result.devssaverurls === undefined) {
            chrome.storage.local.set({'devssaverurls': [urltoset]}, function() {
                console.log("all set1");
            });
            display_warn(urltoset, tab.id);
            return;
        } else {
            
            var index = result.devssaverurls.indexOf(urltoset);
            if (index > -1) {
                result.devssaverurls.splice(index,1);
                chrome.storage.local.set({'devssaverurls': result.devssaverurls }, function() {
                    console.log("all set2");
                });            
                display_warn(urltoset,  tab.id);
            } else {             
                result.devssaverurls.push(urltoset);
                chrome.storage.local.set({'devssaverurls': result.devssaverurls }, function() {
                    console.log("all set3");
                });      
                display_warn(urltoset,  tab.id);
                
            }
        }
        
    });
    console.log(urltoset);
});